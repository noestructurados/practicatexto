# Práctica Texto - Análisis de sentimientos en tweets sobre el COVID-19
Realizado por: Valeria Capurro, Sofía Olalla Fernández de Soto y  Marta Simón Pinacho.

# 1.-Objetivo
El objetivo del proyecto es identificar los sentimientos que transmiten un conjunto de tweets que hablan sobre el COVID-19. Para la realizacion de este proyecto, en primer lugar se ha procedido a realizar una limpieza y análisis de los datos. A continuación, tras el preprocesado, se ha procedido a entrenar una serie de modelos desde 0, desde Naive Bayes, pasando por Random Forest, hasta llegar a entrenar una red neuronal LSTM. Por último, se va a realizar un Fine Tunning de una red neuronal preentrenada llamada Bert Based Uncased, que es utilizada en el análisis de sentimientos en NLP. Se va a reentrenar la red sobre el dataset de tweets que se ha mencionado para comparar los resultados con los modelos desde cero que se han entrenado.

# Limpieza de datos
En un primer notebook se busca entender el tipo de datos que que tiene el dataset. En este caso se quería realizar un análisis de sentimientos en la columna de los tweets, por lo que nos centramos en esa columnas. 

Se realiza una limpieza de los datos, quitando los símbolos estraños y las stopwords, ya que era lo que más se repetia en el dataset. Una vez realizada esa limpieza se vuelven a pintar los gráficos y se puede adivinar que la mayor parte de los tweets hablan de lo que paso durante la pandemia sobretodo en el ambito de los supermercados. 

Además se especifican los tipos de palabras que hay, si son verbos, nombres o adjetivos entre otros. 

# Modelos BOW y TFIDF
El siguiente notebook de Jupyter implementa un sistema de análisis de sentimientos basado en dos enfoques: Bag of Words y TF-IDF. Se realiza un preprocesamiento exhaustivo del texto, que incluye la eliminación de etiquetas HTML, el manejo de ruido textual y el uso de técnicas de stemming para reducir las palabras a su forma raíz. Además, se eliminan las palabras irrelevantes mediante el uso de stop words.

Se utilizan cuatro modelos diferentes para el análisis de sentimientos: Regresión Logística, Random Forest, Naïve Bayes Multinomial y una Red Neuronal. Estos modelos se evalúan y se comparan utilizando métricas como precisión y recall. Se busca determinar qué enfoque (Bag of Words o TF-IDF) y qué modelo produce los resultados más prometedores en términos de clasificación de sentimientos en los tweets.

Asimismo, se probo combinar ambos inputs, el de Bag of Words con el de TFIDF para ver si de esta manera podiamos obtener mejores resultados.

# Red BERT preentrenada
En este último notebook, en primer lugar se realiza una limpieza de los datos. Eliminando emojis, limpiando hashtags y múltiples espacios, para dar paso al preprocesamiento. Una vez limpios los datos se analiza la estructura de los tweets, revisando su longitud para comprobar si, tras la limpieza, ha quedado una oración demasiado pequeña de la que poder extraer cualquier tipo de sentimiento. A partir de este momento, se trabajará únicamente con los tweets que tengan más de 5 palabras.

Un siguiente paso es realizar la tokenización de las oraciones. Para esto se ha empleado el tokenizer de HuggingFace bert-base-uncased. Una vez tokenizados tanto el dataset de train, como el de test, se procede a dividir el dataset de train en un 80-20 train-validación, para poder disponer de tres subconjuntos de datos en total: train, validación y test.

A partir de este momento, se pasa a reentrenar el modelo bert-base-uncased. En este caso, se han añadido dos capas de entrada y una capa densa a modo de clasificador como salida.

Una vez entrenado el modelo, se realiza un predict sobre el conjunto de test y validación para observar los resultados. Se puede ver que, al compararlos con las redes generadas desde cero, da mejores resultados.

